<?php
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev'); //TODO: 在哪里体现

require(__DIR__ . '/MoCha/common/common.php');
require(__DIR__ . '/MoCha/vendor/autoload.php');
require(__DIR__ . '/MoCha/vendor/yiisoft/yii2/Yii.php');
//require(__DIR__ . '/MoCha/common/config/aliases.php'); //TODO: 在哪里体现

$config = require(__DIR__ . '/MoCha/config/web.php');

$application = new yii\web\Application($config);
$application->run();
