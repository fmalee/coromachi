<?php

namespace app\components;

/**
 * Settings Component
 * In your configuration file, add the setting component.
 *  ```php
 * 'components' => [
 * 	...
 * 	'setting' => 'app\components\setting',
 * 	...
 * ]
 * ```
 * 
 * In anywhere from your code, you can use those features:
 * 
 * ```php
 * $setting = Yii::$app->setting->get('category', 'key', 'default value');
 * $setting = Yii::$app->setting->set('category', 'key', 'new value');
 * ```
 * 
 * Or you can query all setting as one
 * 
 * ```php
 * $settingArray = Yii::$app->setting->get('category', 'key');
 * $settingArray = Yii::$app->setting->get('category', 'key', 'default value');
 * $settingArray = Yii::$app->setting->set('category', [
 * 	'key1' => 'value1',
 * 	'key2' => 'value2',
 * 	'key3' => 'value3',
 * ]);
 * ```
 */
use Yii;
use yii\base\Component;
use app\models\Config as Model;
use yii\base\Application;

class Config extends Component
{

	protected $_toBeSave = [];
	protected $_toBeDelete = [];
	protected $_catToBeDel = [];
	protected $_cacheFlush = [];
	protected $_items = [];

	/**
	 * Config::init()
	 *
	 * @return
	 *
	 */
	public function init()
	{
		Yii::$app->on(Application::EVENT_AFTER_REQUEST, [$this, 'commit']);
	}

	/**
	 * Config::set()
	 *
	 * @param string $category
	 * @param mixed $key
	 * @param string $value
	 * @param bool $toDatabase
	 * @return
	 *
	 */
	public function set($category = 'system', $key, $value = '', $toDatabase = true)
	{
		if (is_array($key)) {
			foreach ($key as $k => $v) {
				$this->set($category, $k, $v, $toDatabase);
				Yii::trace($v);
			}
		} else {
			if ($toDatabase) {
				$this->_toBeSave[$category][$key] = $value;
			}
			//$this->_items[$category][$key] = $value;
			$this->load($category, $key, $value);
		}
	}

	/**
	 * Config::get()
	 *
	 * @param string $category
	 * @param string $key
	 * @param string $default
	 * @return
	 *
	 */
	public function get($category = 'system', $key = '', $default = '')
	{
		$this->load($category);

		if (empty($key) && empty($default) && !empty($category)) {
			return isset($this->_items[$category]) ? $this->_items[$category] : null;
		}

		if (isset($this->_items[$category][$key])) {
			return $this->_items[$category][$key];
		}

		return !empty($default) ? $default : null;
	}

	/**
	 * Setting::delete()
	 *
	 * @param string $category
	 * @param string $key
	 * @return
	 *
	 */
	public function delete($category = 'system', $key = '')
	{
		if (!empty($category) && empty($key)) {
			$this->_catToBeDel[] = $category;
			return;
		}
		if (is_array($key)) {
			foreach ($key as $k) {
				$this->delete($category, $k);
			}
		} else {
			$this->load($category);
			if (isset($this->_items[$category][$key])) {
				unset($this->_items[$category][$key]);
				//dump($this->_items);
				$this->_toBeDelete[$category][] = $key;
			}
		}
	}

	/**
	 * Setting::toArray()
	 *
	 * @return
	 *
	 */
	public function toArray()
	{
		return $this->_items;
	}

	/**
	 * Setting::load()
	 *
	 * @param mixed $category
	 * @return
	 *
	 */
	private function load($category, $key = '', $value = '')
	{
		
		if (!isset($this->_items[$category])) {
			$items = $this->cache($category);
		


			if (!$items) {
				$items = [];

				$result = Model::find()->where(['category' => $category])->all();
				if (!empty($result)) {

					foreach ($result as $row) {
						$items[$row->key] = @unserialize($row->value);
					}
				}
				//$this->_items[$category] = $items;
				//Yii::$app->cache->add($category . '_setting', $items);
			}
			if ($key && $value) {
				$items[$key] = $value;
				
				//dump($items);
				
				
			}
			if ($items) {
				$this->_items[$category] = $items;
				$this->cache($category, null);
				$this->cache($category, $items);
			}
			
			//$this->set($category, $items, '', false);
		}
		

		return $this->_items;
	}

	/**
	 * Setting::cache()
	 *
	 * @param string $category
	 * @param mixed $items
	 * @return
	 *
	 */
	private function cache($category = 'system', $items = '')
	{
		$category = $category . '_config';
		if (is_array($items)) {
			Yii::$app->cache->add($category, $items);
		} elseif (is_null($items)) {
			Yii::$app->cache->delete($category);
		} else {
			Yii::$app->cache->add($category, $items);
		}
	}

	/**
	 * Setting::addDbItem()
	 *
	 * @param string $category
	 * @param mixed $key
	 * @param mixed $value
	 * @return
	 *
	 */
	private function addDbItem($category = 'system', $key, $value)
	{
		$model = Config::find()->where(['category'=>$category, 'key'=>$key])->one();
		
		if (!$model) {
			$model = new Setting();
		}
		$model->category = $category;
		$model->key = $key;
		$model->value = @serialize($value);
		$model->save();
	}

	/**
	 * @return
	 *
	 */
	public function commit()
	{
		$this->_cacheFlush = [];
		if (count($this->_catToBeDel) > 0) {
			foreach($this->_catToBeDel as $catName) { //删除整个类目
				Config::find()->where(['category' => $catName])->delete(); //从数据库删除
				$this->_cacheFlush [] = $catName;

				if (isset($this->_toBeDelete[$catName])) {
					unset($this->_toBeDelete[$catName]);
				}
				if (isset($this->_toBeSave[$catName])) {
					unset($this->_toBeSave[$catName]);
				}
			}
		}
		//dump($this->_toBeDelete);
		if (count($this->_toBeDelete) > 0) {  //删除单个值
			foreach ($this->_toBeDelete as $catName => $keys) {
				//dump($this->_toBeDelete);
				$params = [];
				foreach ($keys as $v) {
					if (isset($this->_toBeSave[$catName][$v])) {
						unset($this->_toBeSave[$catName][$v]);
					}
					$params[$v] = $v;
				}

				$model = Config::deleteAll(['category' => $catName, 'key' => $params]);  //TODO

				$this->_cacheFlush[] = $catName;
			}
		}

		/** @FIXME: Switch to batch mode... **/
		if (count ($this->_toBeSave) > 0) {
			foreach ($this->_toBeSave as $catName => $data) {
				foreach ($data as $key => $value) {
					$this->addDbItem($catName, $key, $value);
				}
				//$this->_cacheFlush[] = $catName;
			}
		}

		if (count ($this->_cacheFlush) > 0) { //删除对应缓存
			foreach ($this->_cacheFlush as $catName) {
				Yii::$app->cache->delete($catName . '_setting');
			}
		}
	}
}
