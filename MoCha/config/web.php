<?php

Yii::setAlias('@mdm', dirname(__DIR__) . '\\modules'); //admin
$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'InMoCha',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(__DIR__) . '/vendor',
    'extensions' => require(__DIR__ . '/../vendor/yiisoft/extensions.php'),
    'controllerNamespace' => 'app\controllers',
    //'language' => 'zh_CN',
    'timeZone'=>'Asia/Shanghai',
    'components' => [
        //'config' => 'app\components\Config',
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            //'suffix' => '.html',
            'rules' => [
                '<action:(login|logout|register)>' => 'account/default/<action>',
                'article/<category:\w+>' => 'article/list',
                'article/<category:\w+>/<id:\d+>' => 'article/view',
                //'reset/<id:\d+>/<token:\w+>' => 'user/recovery/reset',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' =>'/account/login'
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mail' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'sourcePath' => null,
                    'css' => [],
                ],
                // 'yii\bootstrap\BootstrapPluginAsset' => [
                //  'sourcePath' => null,
                //  'js' => []
                // ],
                // 'yii\web\JqueryAsset' => [
                //  'sourcePath' => null,
                //  'js' => []
                // ]
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ]
    ],
    //'as access' => 'mdm\admin\components\AccessControl', //admin
    'aliases' => [],
    'modules' => [
        'account' => 'app\modules\account\Module',
        'admin' => 'mdm\admin\Module', //admin
    ],
    'params' => $params,
];

if (YII_DEBUG) {
    // configuration adjustments for 'dev' environment
    $config['preload'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';
    $config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
