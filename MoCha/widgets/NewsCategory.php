<?php

namespace app\widgets;

use yii\base\Widget;
use app\models\Category;
use yii\widgets\Menu;
use yii\bootstrap\Nav;
/**
 * 根据ID获取栏目导航
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class NewsCategory extends Widget
{
	public $category_id; //父栏目ID
	public $module_id;
	public $items;

	public function init()
	{
		if (!$this->category_id) {
			throw new CException(必须指定一个栏目ID);
		}
	}

	public function run()
	{
		echo Nav::widget([
			'items' => $this->getcategorys(),
			'options' => ['class' => 'nav-pills']
		]);
	}

	public function getcategorys()
	{
		$categorys = Category::getTree($this->category_id);
		$categorys = array_shift($categorys);

		foreach ($categorys as $category)
        {
           //$childItems=$this->getItems($menu_id, $result['item_id']); 
            $items[] = array(
               'label' => $category['title'],
               'url' => ['/article/list', 'category' => $category['name']],
             );
        }

        return $items;
	}

}

?>