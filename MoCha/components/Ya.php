<?php

namespace app\components;

use Yii;
use app\models\Config;
use yii\helpers\BaseHtml;
use yii\helpers\BaseUrl as Url;

/**
 * Helper class
 * @author Joey<lixueli@gmail.com>
 * @since 2.0
 *
 */
class Ya extends BaseHtml
{
    public static function C($key, $group = Config::GROUP_SYSTEM)
    {
        static $_configs = [];
        $cache = $group . '_config';

        if (!isset($_configs[$group])) {
            $items = Yii::$app->cache->get($cache);
            if (!$items) {
                $result = \app\models\Config::find()->where(['group' => $group])->all();

                if (!empty($result)) {
                    $items = [];
                    foreach ($result as $row) {
                        $items[$row->name] = $row->value;
                    }
                    
                    $_configs[$group] = $items;
                    Yii::$app->cache->add($cache, $items);
                } else {
                    $_configs[$group] = [];
                }
            } else {
                $_configs[$group] = $items;
            }
        }
        if (isset($_configs[$group][$key])) {
            return $_configs[$group][$key];
        }
        return null;
    }

    /**
     * 便捷操作session的flash.
     * @return array flash messages (key => message).
     */
    public static function flash($key, $value = '')
    {
        if (empty($key)) {
            return Yii::$app->getSession()->getAllFlashes();
        }

        if (is_null($value)) {
            return Yii::$app->getSession()->removeFlash($key);
        } elseif (!empty($value)) {
            return Yii::$app->getSession()->setFlash($key, $value);
        } else {
            return Yii::$app->getSession()->getFlash($key);
        }
    }

    /**
     * Returns URL for a route.
     *
     * @param array|string $route route as a string or route and parameters in form of
     *                            `['route', 'param1' => 'value1', 'param2' => 'value2']`.
     *
     * If there is a controller running, relative routes are recognized:
     *
     * - If the route is an empty string, the current [[\yii\web\Controller::route|route]] will be used;
     * - If the route contains no slashes at all, it is considered to be an action ID
     *   of the current controller and will be prepended with [[\yii\web\Controller::uniqueId]];
     * - If the route has no leading slash, it is considered to be a route relative
     *   to the current module and will be prepended with the module's uniqueId.
     *
     * In case there is no controller, [[\yii\web\UrlManager::createUrl()]] will be used.
     *
     * @param boolean|string $scheme URI scheme to use:
     *
     * - `false`: relative URL. Default behavior.
     * - `true`: absolute URL with the current scheme.
     * - string: absolute URL with string value used as scheme.
     *
     * @return string                the URL for the route
     * @throws InvalidParamException if the parameter is invalid.
     */
    public static function toRoute($route, $scheme = false)
    {
        return Url::toRoute($route, $scheme);
    }

    /**
     * Formats the value based on the given format type.
     * This method will call one of the "as" methods available in this class to do the formatting.
     * For type "xyz", the method "asXyz" will be used. For example, if the format is "html",
     * then [[asHtml()]] will be used. Format names are case insensitive.
     * @param  mixed                 $value  the value to be formatted
     * @param  string|array          $format the format of the value, e.g., "html", "text". To specify additional
     *                                       parameters of the formatting method, you may use an array. The first element of the array
     *                                       specifies the format name, while the rest of the elements will be used as the parameters to the formatting
     *                                       method. For example, a format of `['date', 'Y-m-d']` will cause the invocation of `asDate($value, 'Y-m-d')`.
     * @return string                the formatting result
     * @throws InvalidParamException if the type is not supported by this class.
     */
    public static function formatter($value, $format = null)
    {
        return Yii::$app->formatter->format($value, $format);
    }

    /**
     * Formats the value as a hyperlink.
     * @param  mixed  $value the value to be formatted
     * @return string the formatted result
     */
    public static function asUrl($value)
    {
        return Yii::$app->formatter->asUrl($value);
    }

    /**
     * Formats the value as a date.
     * @param integer|string|DateTime $value the value to be formatted. The following
     *                                       types of value are supported:
     *
     * - an integer representing a UNIX timestamp
     * - a string that can be parsed into a UNIX timestamp via `strtotime()`
     * - a PHP DateTime object
     *
     * @param  string $format the format used to convert the value into a date string.
     *                        If null, [[dateFormat]] will be used. The format string should be one
     *                        that can be recognized by the PHP `date()` function.
     * @return string the formatted result
     * @see dateFormat
     */
    public static function asDate($value, $format = null)
    {
        return Yii::$app->formatter->asDate($value, $format);
    }

    /**
     * Formats the value as a datetime.
     * @param integer|string|DateTime $value the value to be formatted. The following
     *                                       types of value are supported:
     *
     * - an integer representing a UNIX timestamp
     * - a string that can be parsed into a UNIX timestamp via `strtotime()`
     * - a PHP DateTime object
     *
     * @param  string $format the format used to convert the value into a date string.
     *                        If null, [[datetimeFormat]] will be used. The format string should be one
     *                        that can be recognized by the PHP `date()` function.
     * @return string the formatted result
     * @see datetimeFormat
     */
    public static function asDatetime($value, $format = null)
    {
        return Yii::$app->formatter->asDatetime($value, $format);
    }

    public static function hint($message) {
        return '<div class="hint">' . Ya::t($message) . '</div>';
    }

    //获取、设置配置参数
    public static function param($params) {
        return Yii::app()->params[$params];
    }

    public static function p($string, $params = array())
    {
        return '<p>' . Ya::t($string, $params) . '</p>';
    }

    /** Fetch the translation string from db and cache when necessary */
    public static function t($str='', $params=array(), $dic='MoCha')
    {
        return Yii::t($dic, $str, $params);
    }

    // returns the Yii User Management module. Frequently used for accessing
    // options by calling Yum::module()->option
    public static function module($module = 'seller') {
        return Yii::app()->getModule($module);
    }

    public static function hasModule($module) {
        return array_key_exists($module, Yii::app()->modules);
    }

    /**
     * 记录操作日志
     *
     * @param string $type
     * @param string $reason
     * @param string $content
     *
     */
    public function event($module, $type, $reason, $content)
    {
        $event = new Event;
        $event->module = $module;
        $event->type = $type;
        $event->reason = $reason;
        $event->content = $content;
        $$event->save();

        return true;
    }

    /**
     * 转换Validate格式
     * @param string $validate
     */
    public static function formatValidate($validate)
    {
        $result = '';
        $validates = json_decode($validate, true);
        foreach ($validates as $field) {
            foreach ($field as $item) {
                $result .='<p>'.$item.'</p>';
            }
        }

        return $result;
    }

    /**
     * 注册JS.CSS资源
     * @param array $files
     */
    public static function register($files) {
        if(is_array($files))
        {
            foreach ($files as $file) {
                if(strpos($file, 'js') !== false)
                    $url = Yii::app()->params->jsPath;
                else
                    $url = Yii::app()->params->cssPath;

                if(strpos($file,'/') > 0 || strpos($file,'/') === false)
                    $path = $url.$file;

                if(strpos($file, 'js') !== false)
                    Yii::app()->clientScript->registerScriptFile($path, CClientScript::POS_END);
                else if(strpos($file, 'css') !== false)
                    Yii::app()->clientScript->registerCssFile($path);
            }
        }
    }

    /**
     * 注册CSS代码资源
     * @param string $id
     * @param string $css
     * @param bool $iscode
     */
    public static function registerCss($id, $css, $iscode = false)
    {
        if(!$iscode)
            $css = Ya::renderFile($css);
        Yii::app()->clientScript->registerCss($id, $css);
    }

    /**
     * 注册JS代码资源
     * @param string $id
     * @param string $script
     * @param bool $iscode
     */
    public static function registerScript($id, $script, $iscode = false)
    {
        if(!$iscode)
            $script = Ya::renderFile($script);
        Yii::app()->clientScript->registerScript($id, $script, CClientScript::POS_END);
    }

    /**
     * 返回渲染的视图
     *
     * @param string $view
     * @param array $data
     * @param bool $return
     * @param bool $processOutput
     *
     */
    public static function renderFile($view, $data=null, $return=true, $processOutput=false)
    {
        $cs = Yii::app()->getController();
        $path = $cs->getViewPath().DIRECTORY_SEPARATOR;
        return $cs->renderFile($path.$view, $data, $return, $processOutput);
    }

}
