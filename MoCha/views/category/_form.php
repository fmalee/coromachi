<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Category $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 30]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'template_index')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'template_lists')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'template_detail')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'template_edit')->textInput(['maxlength' => 100]) ?>

    <?= $form->field($model, 'parent_id')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'icon')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'module')->textInput() ?>

    <?= $form->field($model, 'page_size')->textInput() ?>

    <?= $form->field($model, 'allow_publish')->textInput() ?>

    <?= $form->field($model, 'display')->textInput() ?>

    <?= $form->field($model, 'reply')->textInput() ?>

    <?= $form->field($model, 'check')->textInput() ?>

    <?= $form->field($model, 'sort')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'created')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'modified')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'meta_title')->textInput(['maxlength' => 50]) ?>

    <?= $form->field($model, 'arr_parent')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'keywords')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
