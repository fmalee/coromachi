<?php

use app\components\Ya;
use yii\helpers\Html;
$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
$this->params['body'] = 'page-category';
?>
                    <h2 class="heading_title"><span>网站公告</span></h2>
                    <div class="category-list clearfix">
                        <ul class="col-md-12">
                            <?php foreach ($models as $model): ?>
                            <li>
                                <div class="col-lg-1"><a href="<?= Ya::toRoute(['/article/list/', 'category'=>$model->category->name]) ?>"><span class="label label-primary"><?= $model->category->name ?></span></a></div>
                                <div class="col-lg-10"><?= Ya::a($model->title, ['/article/view', 'id' => $model->id, 'category' => $model->category->name]) ?></div>
                                <div class="col-lg-1"><span class="badge pull-right"><?= Ya::asDate($model->modified) ?></span></div>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
