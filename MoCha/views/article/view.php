<?php

use app\components\Ya;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['body'] = 'page-category';
?>
                    <h1><?=$model->title?></h1>
                    <div class="category-info clearfix">
                        <div class="image">
                            <img src="/Statics/images/demo/img-blog5-900x350w.jpg" alt="Mac" />
                        </div>
                        <div class="description">
                            <p><?=$model->description?></p>
                            <ul class="list-inline">
                                <li><span class="fa fa-clock-o"><?=$model->getAttributeLabel('modified')?>:</span> <?=Yii::$app->formatter->asDate($model->modified)?></li>
                                <li><span class="fa fa-pencil"><?=$model->getAttributeLabel('user_id')?>:</span> <?=Ya::encode($model->user->username)?></li>
                                <li><span class="fa fa-eye"><?=$model->getAttributeLabel('view')?>:</span> <?=Ya::encode($model->view)?></li>
                                <li><span class="fa fa-comments"><?=$model->getAttributeLabel('comment')?>:</span> <?=Ya::encode($model->comment)?></li>
                                <li><span class="fa fa-comments"><?=$model->getAttributeLabel('category_id')?>:</span> <?=Ya::encode($model->category->title)?></li>
                            </ul>
                            
                        </div>
                    </div>
                    <div class="category-list clearfix">
                        <?=$model->content?>
                    </div>
