<?php

use app\components\Ya;
use yii\helpers\Html;
use yii\widgets\Menu;
use app\widgets\NewsCategory;

$this->title = $category->title;
$this->params['breadcrumbs'][] = $this->title;
$this->params['body'] = 'page-category';
?>
            <section class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <!--<div class="success">这是一个提示信息</div>-->
                <div id="content">
                    <div class="category-info clearfix">
                        <div class="image"><img src="/statics/images/demo/img-category-873x250.jpg" alt="Mac" /></div>
                    <h2 class="heading_title"><span><?= $category->title ?></span></h2>
                        <div class="description">
                            <?= NewsCategory::widget(['category_id' => 1]) ?>

                        </div>
                    </div>
                    <div class="category-list clearfix">
                        <ul class="col-md-12">
                            <?php foreach ($models as $model): ?>
                            <li>
                                <?php if($category->childs):?>
                                    <div class="col-lg-1"><a href="<?= Ya::toRoute(['/article/list/', 'category'=>$model->category->name]) ?>"><span class="label label-primary"><?= $model->category->title ?></span></a></div>
                                <?php endif;?>
                                <div class="col-lg-10"><?= Ya::a($model->title, ['/article/view', 'id' => $model->id, 'category' => $model->category->name]) ?></div>
                                <div class="col-lg-1"><span class="badge pull-right"><?= Ya::asDate($model->modified) ?></span></div>
                            </li>
                            <?php endforeach;?>
                        </ul>
                    </div>
                        
                    
                </div>
            </section>
            <aside class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div id="column-left" class="sidebar">
                    <div id="column-right" class="sidebar">
                        




                        <div class="box">
                            <div class="box-heading"><span>交易记录</span></div>
                            <div class="box-content  col-lg-12">
                                <?= Menu::widget(array(
                                    'items'=>array(
                                        array(
                                            'label'=>'配货管理',
                                            'url'=>array('/seller/invoice'),
                                            'options'=>array('css' => 'haschild'),
                                        ),
                                        array(
                                            'label'=>'刷单管理',
                                            'url'=>array('/seller/fake'),
                                            'options'=>array('css' => 'haschild'),
                                        ),
                                        array(
                                            'label'=>'售后服务',
                                            'url'=>array('/seller/service'),
                                            'options'=>array('css' => 'haschild'),
                                            'items'=>array(
                                                array('label'=>'换货记录', 'url'=>array('/seller/service/niffer'), 'active'=> true),
                                                array('label'=>'换货记录', 'url'=>array('/seller/service/niffer'), 'linkOptions'=>array('class'=>'corner-all')),
                                                array('label'=>'换货记录', 'url'=>array('/seller/service/niffer'), 'linkOptions'=>array('class'=>'corner-all')),
                                                array('label'=>'换货记录', 'url'=>array('/seller/service/niffer'), 'linkOptions'=>array('class'=>'corner-all')),
                                            )
                                        ),
                                    ),
                                    'submenuTemplate' => "\n<ul class=\"list\">\n{items}\n</ul>\n",
                                    //'labelTemplate' => '{label}',
                                    //'itemOptions'=>array('css' => 'box-category list'),
                                    'options'=>array('class'=>'box-category list'),
                                    //'encodeLabels'=>false
                                )); ?>
                            </div>
                            <div class="box-heading"><span>会员中心</span></div>
                            <div class="box-content">
                                <ul class="list">
                                    <li class="active"><?= Ya::a('退出登录', ['/account/default/logout']) ?></li>
                                    <li><?= Ya::a('验证邮箱', ['/account/confirm/']) ?></li>
                                    <li><?= Ya::a('个人资料', ['/account/profile/']) ?></li>
                                    <li><?= Ya::a('修改资料', ['/account/settings/']) ?></li>
                                    <li><?= Ya::a('修改邮箱', ['/account/settings/email']) ?></li>
                                    <li><?= Ya::a('修改密码', ['/account/settings/password']) ?></li>
                                </ul>
                            </div>

                            <div class="box-heading"><span>个人设置</span></div>
                            <div class="box-content">
                                <ul class="list">
                                    <li><?= Ya::a('个人资料', ['/account/settings/']) ?></li>
                                    <li><?= Ya::a('验证邮箱', ['/account/confirm/']) ?></li>
                                    <li><?= Ya::a('修改邮箱', ['/account/settings/email']) ?></li>
                                    <li><?= Ya::a('修改密码', ['/account/settings/password']) ?></li>
                                    <li><?= Ya::a('退出登录', ['/account/default/logout']) ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
            

