<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\Article $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'category_id')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'link_id')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'cover_id')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'display')->textInput() ?>

    <?= $form->field($model, 'attach')->textInput() ?>

    <?= $form->field($model, 'view')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'comment')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'level')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'created')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'modified')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => 40]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 80]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => 140]) ?>

    <?= $form->field($model, 'template')->textInput(['maxlength' => 100]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
