<?php
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var \frontend\models\ResetPasswordForm $model
 */
$this->title = '首页';
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1>Account Login</h1>
    <div class="login-content">
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="inner">
                    <h2>New Customer</h2>
                    <div class="content">
                        <p><b>Register Account</b></p>
                        <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made.</p>
                        <a href="http://demopavothemes.com/pav_digital_store/index.php?route=account/register" class="button">Continue</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="inner">
                    <h2>Returning Customer</h2>
                    <form action="http://demopavothemes.com/pav_digital_store/index.php?route=account/login" method="post" enctype="multipart/form-data">
                        <div class="content">
                            <p>I am a returning customer</p>
                            <b>E-Mail Address:</b>
                            <br />
                            <input class="form-control" type="text" name="email" value="" />
                            <br />
                            <br />
                            <b>Password:</b>
                            <br />
                            <input class="form-control" type="password" name="password" value="" />
                            <br />
                            <a href="http://demopavothemes.com/pav_digital_store/index.php?route=account/forgotten">Forgotten Password</a>
                            <br />
                            <br />
                            <input type="submit" value="登录" class="button" />
                            <input type="hidden" name="redirect" value="" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php $this->beginBlock('sidebar') ?>
<div class="box">
    <h3 class="box-heading"><span>Blog Category</span></h3>
    <div class="box-content" id="pav-categorymenu">
        <ul class="level1 pav-category ">
            <li>
                <a href="category&amp;id=22" title="Bibendum sem">Bibendum sem</a>
                <span class="head selected"><a style="float:right;" href="#">+</a></span>
                <ul class="level2" style="display: block;">
                    <li><a href="category&amp;id=21" title="Vestibulum massa">Vestibulum massa</a></li>
                    <li><a href="category&amp;id=23" title="Placerat egestas">Placerat egestas</a></li>
                    <li><a href="category&amp;id=20" title="Pharetra ultrices">Pharetra ultrices</a></li>
                </ul>
            </li>
            <li><a href="category&amp;id=24" title="Turpis ipsum">Turpis ipsum</a></li>
            <li><a href="category&amp;id=25" title="Sed fermentum">Sed fermentum</a></li>
            <li><a href="category&amp;id=26" title="Fusce cursus">Fusce cursus</a></li>
            <li><a href="category&amp;id=27" title="Curabitur faucibus">Curabitur faucibus</a></li>
        </ul>
    </div>
</div>
<script>
    $(document).ready(function(){
        $("#pav-categorymenu ul").addClass("list");
            // applying the settings
            $("#pav-categorymenu li.active span.head").addClass("selected");
                $('#pav-categorymenu ul').Accordion({
                    active: 'span.selected',
                    header: 'span.head',
                    alwaysOpen: false,
                    animated: true,
                    showSpeed: 400,
                    hideSpeed: 800,
                    event: 'click'
                });
    });
</script>
<?php $this->endBlock() ?>
<?php $this->beginBlock('css') ?>
    <link href="<%=Ya::C('jsPath')%>/jquery/colorbox/colorbox.css" rel="stylesheet" />
<?php $this->endBlock() ?>
<?php $this->beginBlock('js') ?>
    <script type="text/javascript" src="<%=Ya::C('jsPath')%>/jquery/pavblog_script.js"></script>
<?php $this->endBlock() ?>