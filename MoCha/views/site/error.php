<?php
use yii\helpers\Html;

$this->title = $name;
?>
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="content">
        <blockquote>
            <p><b><?= nl2br(Html::encode($message)) ?></b></p>
            <small>
            很抱歉，我们没有找到有效的请求结果。<br />
            请确认您的链接是否正确，或是联系管理求助。
            </small>
        </blockquote>
    </div>
    <div class="buttons">
        <div class="right"><a href="/" class="button">会员中心</a> <a href="/" class="button">返回首页</a></div>
    </div>