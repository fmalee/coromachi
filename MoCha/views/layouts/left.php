<?php $this->beginContent('@app/views/layouts/main.php')?>
            <aside class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                <div id="column-left" class="sidebar">
                    <?= $this->blocks['sidebar'] ?> <!--sidebar片断-->
                </div>
            </aside>
            <section class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                <!--<div class="success">这是一个提示信息</div>-->
                <div id="content">
                    <?= $content ?>
                </div>
            </section>
<?php $this->endContent() ?>