<?php $this->beginContent('@app/views/layouts/main.php')?>
            <section class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <!--<div class="success">这是一个提示信息</div>-->
                <div id="content">
                    <?= $content?>
                </div>
            </section>
            <aside class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div id="column-right" class="sidebar">
                    <?= $this->blocks['sidebar']?> <!--sidebar片断-->
                </div>
            </aside>
<?php $this->endContent()?>