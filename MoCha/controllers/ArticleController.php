<?php

namespace app\controllers;

use Yii;
use app\componets\Ya;
use app\models\Article;
use app\models\ArticleSearch;
use app\models\Category;
use app\controllers\BaseController;
use yii\web\NotFoundHttpException;
use yii\web\VerbFilter;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends BaseController
{
    public $layout = '/main';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex($category = '')
    {
        $where['module'] = Category::MODULE_ARTICLE;
        if (!empty($category)) {
            $where['name'] = $category;
        }
        $categorys = Category::find()->where($where)->column();
        $models = Article::find()->where(['status' => Article::STATUS_ACTIVE, 'category_id' => $categorys])->with('category')->all();
        //dump($models);

        return $this->render('index', [
            'models' => $models,
        ]);
    }

    /**
     * Lists sub category with Article models.
     * @return mixed
     */
    public function actionList($category, $page = 1)
    {
        $cat = Category::findByName($category);
        if ($cat) {
            $view = $cat->template_lists ?: 'list';

            if ($cat->childs == 0) {
                $categorys = $cat->id;
            } else {
                $categorys = Category::find()
                    ->where(['like', 'parents', $cat->id . '|'])
                    ->andWhere(['childs' => 0])
                    ->column();
            }
            
            $models = Article::find()
                ->where(['status' => Article::STATUS_ACTIVE, 'category_id' => $categorys])
                ->with('category')
                ->all();

            return $this->render($view, [
                'models' => $models,
                'category' => $cat,
            ]);
        } else {
            throw new NotFoundHttpException('亲请求的页面找不到了喔！');
        }
    }

    public function actionA()
    {
        return $this->renderPartial('a');
    }

    /**
     * Displays a single Article model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id, $category)
    {
        $model = $this->findModel($id);
        if ($model->category && ($model->category->name == $category)) {
            $view = $model->category->template_detail ?: 'view';
            return $this->render($view, [
                'model' => $model,
            ]);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if ($id !== null && ($model = Article::find($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
