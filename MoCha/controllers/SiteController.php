<?php
namespace app\controllers;

use Yii;
use app\components\Ya;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    public $layout='single';

    public function actionIndex()
    {
        return $this->render('index');
    }
}
