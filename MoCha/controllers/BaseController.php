<?php
namespace app\controllers;

use yii\web\Controller;
use Yii;

/**
 * Site controller
 */
class BaseController extends Controller
{
    public $layout='/left';
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function init()
    {
        parent::init();
        Yii::$container->set('yii\widgets\LinkPager', [
            'yii\widgets\LinkPager' => [
                'options' => [
                    'class' => 'pagination',
                ],
            ],
        ]);
    }
}
