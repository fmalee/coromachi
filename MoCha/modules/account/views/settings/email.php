<?php

use yii\helpers\Url;
use yii\helpers\Html;
$this->title = '电子邮箱设置';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<?php if (Yii::$app->getSession()->hasFlash('settings_saved')): ?>
		<div class="col-md-12">
			<div class="alert alert-success">
				<?= Yii::$app->getSession()->getFlash('settings_saved') ?>
			</div>
		</div>
	<?php endif; ?>
	<div class="col-md-12">
		<div class="inner">
            <div class="col-md-12 text-center"><h1><?= Html::encode($this->title) ?></h1></div>
			<div class="content">
				<?php if (!empty($model->unconfirmed_email)): ?>
					<div class="alert alert-warning">
                        如果要换新邮箱，我们需要重新认证，请认真对待。
						<?= \yii\helpers\Html::a('取消对邮箱的更改', Url::to(['reset']), ['class' => 'btn btn-danger btn-xs', 'data-method' => 'post']) ?>
					</div>
				<?php endif; ?>
				<?php $form = \yii\widgets\ActiveForm::begin([
					'id' => 'profile-form',
					'options' => ['class' => 'form-horizontal'],
					'fieldConfig' => [
						'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
						'labelOptions' => ['class' => 'col-lg-3 control-label'],
					],
				]); ?>

				<div class="form-group">
					<label class="col-lg-3 control-label">当前邮箱</label>
					<div class="col-lg-9">
						<p class="form-control-static"><?= $model->email ?></p>
					</div>
				</div>

				<?= $form->field($model, 'unconfirmed_email') ?>

				<?= $form->field($model, 'current_password')->passwordInput() ?>

				<div class="form-group">
					<div class="col-lg-offset-3 col-lg-9">
						<?= \yii\helpers\Html::submitButton('更新邮箱', ['class' => 'btn btn-success']) ?><br>
					</div>
				</div>

				<?php \yii\widgets\ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
<?php $this->beginBlock('sidebar') ?>
<div id="column-right" class="sidebar">
    <div class="box">
        <div class="box-heading"><span>交易记录</span></div>
        <div class="box-content">
            <ul class="list">
                <li><?= Html::a('我的订单', ['/account/default/logout'], ['class'=>'active']) ?></li>
                <li><?= Html::a('我的收藏', ['/account/confirm/']) ?></li>
                <li><?= Html::a('淘宝订单', ['/account/confirm/']) ?></li>
            </ul>
        </div>
        <div class="box-heading"><span>会员中心</span></div>
        <div class="box-content">
            <ul class="list">
                <li class="active"><?= Html::a('退出登录', ['/account/default/logout']) ?></li>
                <li><?= Html::a('验证邮箱', ['/account/confirm/']) ?></li>
                <li><?= Html::a('个人资料', ['/account/profile/']) ?></li>
                <li><?= Html::a('修改资料', ['/account/settings/']) ?></li>
                <li><?= Html::a('修改邮箱', ['/account/settings/email']) ?></li>
                <li><?= Html::a('修改密码', ['/account/settings/password']) ?></li>
            </ul>
        </div>
        
        <div class="box-heading"><span>个人设置</span></div>
        <div class="box-content">
            <ul class="list">
                <li><?= Html::a('个人资料', ['/account/settings/']) ?></li>
                <li><?= Html::a('验证邮箱', ['/account/confirm/']) ?></li>
                <li><?= Html::a('修改邮箱', ['/account/settings/email']) ?></li>
                <li><?= Html::a('修改密码', ['/account/settings/password']) ?></li>
                <li><?= Html::a('退出登录', ['/account/default/logout']) ?></li>
            </ul>
        </div>
    </div>
</div>
<?php $this->endBlock() ?>