<?php
use app\components\Ya;
use yii\helpers\Html;

$this->title = '密码设置';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<?php if (Ya::flash('settings_saved')): ?>
		<div class="col-md-12">
			<div class="alert alert-success">
				<?= Ya::flash('settings_saved') ?>
			</div>
		</div>
	<?php endif; ?>
	<div class="col-md-12">
		<div class="inner">
			<div class="col-md-12 text-center"><h1><?= Html::encode($this->title) ?></h1></div>
			<div class="content">
				<?php $form = \yii\widgets\ActiveForm::begin([
					'id' => 'profile-form',
					'options' => ['class' => 'form-horizontal'],
					'fieldConfig' => [
						'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
						'labelOptions' => ['class' => 'col-lg-3 control-label'],
					],
				]); ?>

				<?= $form->field($model, 'current_password')->passwordInput() ?>

				<?= $form->field($model, 'password')->passwordInput() ?>

				<div class="form-group">
					<div class="col-lg-offset-3 col-lg-9">
						<?= \yii\helpers\Html::submitButton('修改密码', ['class' => 'btn btn-success']) ?><br>
					</div>
				</div>

				<?php \yii\widgets\ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>