<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = '设置新密码';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-offset-5 col-lg-7"><h2><?= Html::encode($this->title) ?></h2></div>
<?php $form = ActiveForm::begin([
	'id' => 'recovery-form',
	'options' => ['class' => 'form-horizontal'],
	'fieldConfig' => [
		'template' => "{label}\n<div class=\"col-lg-2\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
		'labelOptions' => ['class' => 'col-lg-5 control-label'],
	],
]); ?>

<?= $form->field($model, 'password')->passwordInput() ?>

	<div class="form-group">
		<div class="col-lg-offset-5 col-lg-7">
			<?= Html::submitButton('设置密码', ['class' => 'btn btn-primary']) ?>
		</div>
	</div>

<?php ActiveForm::end(); ?>