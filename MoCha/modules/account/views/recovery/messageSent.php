<?php

$this->title = '找回密码';
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1>成功发送密码重置邮件</h1>
    <div class="content">
        <blockquote>
            <small>
                我们已经向您的邮箱发送了一份带链接邮件。<br />
                请检查邮箱里的邮件，按照说明重置您的密码。
            </small>
        </blockquote>
    </div>
    <div class="buttons">
        <div class="right"><a href="/" class="button">返回首页</a></div>
    </div>