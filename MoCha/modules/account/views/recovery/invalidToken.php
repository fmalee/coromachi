<?php
use yii\helpers\Html;

$this->title = '密码找回';
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1>很抱歉，您使用了一个无效的重置码。</h1>
    <div class="content">
        <blockquote>
            <small>
                请确认一下您电子邮箱中的重置链接。<br />
            </small>
        </blockquote>
        <?= Html::a('重发密码重置信息', ['/account/recovery/'], ['class'=>'button']) ?>
    </div>
    <div class="buttons">
        <div class="right"><a href="/" class="button">返回首页</a></div>
    </div>