<?php
use yii\helpers\Html;

/**
 * @var yii\base\View $this
 */
$this->title = '密码找回';
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1>密码重置成功</h1>
    <div class="content">
        <blockquote>
            <small>
            现在您可以使用新密码进行登录了。
            </small>
        </blockquote>
    </div>
    <div class="buttons">
        <div class="right"><a href="/" class="button">返回首页</a></div>
    </div>