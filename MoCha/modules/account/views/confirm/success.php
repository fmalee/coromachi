<?php
use yii\helpers\Html;

$this->title = Yii::$app->getModule('account')->confirmable ? '您还需要进一步确认注册信息' : '账号注册成功';
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1><?=$this->title?></h1>
    <div class="content">
        <blockquote>
            <small>
            <?php if (Yii::$app->getModule('account')->confirmable): ?>
                您注册的资料已经提交，不过我们还需要确认您的电子邮箱地址。<br />
                为了完成注册，麻烦您检查一下电子邮箱。<br />
                如果还没有收到确认邮件，请点击下面的链接：<br /><br />
                <?= Html::a('重发注册确认信息', ['/account/confirm/'], ['class'=>'button']) ?>
            <?php else: ?>
                恭喜，您的账户已经注册成功。<br />
                您现在可以用刚才注册资料登录了。
            <?php endif; ?>
            </small>
        </blockquote>
    </div>
    <div class="buttons">
        <div class="right"><a href="/" class="button">返回首页</a></div>
    </div>