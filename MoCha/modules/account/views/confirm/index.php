<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = '注册信息';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-offset-4 col-lg-6"><h2>重新发送注册确认邮件</h2></div>
<?php $form = ActiveForm::begin([
	'id' => 'registration-form',
	'options' => ['class' => 'form-horizontal'],
	'fieldConfig' => [
		'template' => "{label}\n<div class=\"col-lg-2\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
		'labelOptions' => ['class' => 'col-lg-5 control-label'],
	],
]); ?>

<?= $form->field($model, 'email') ?>

<?php if (in_array('resend', Yii::$app->getModule('account')->captcha)): ?>
	<?= $form->field($model, 'verifyCode')->widget(\yii\captcha\Captcha::className(), ['template' => '{input}{image}']) ?>
<?php endif ?>

	<div class="form-group">
		<div class="col-lg-offset-5 col-lg-6">
			<?= Html::submitButton('发 送', ['class' => 'btn btn-primary']) ?>
		</div>
	</div>

<?php ActiveForm::end(); ?>