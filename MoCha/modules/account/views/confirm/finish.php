<?php

$this->title = '用户注册';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alert alert-success">
	<h4>您的账户已经成功激活</h4>
	您现在可以使用之前注册的资料登录了。
</div>