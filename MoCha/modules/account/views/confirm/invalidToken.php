<?php
use yii\helpers\Html;

/**
 * @var yii\base\View $this
 */
$this->title = '用户注册';
$this->params['breadcrumbs'][] = $this->title;
?>
    <h1>很抱歉，您使用了一个无效的注册确认码。</h1>
    <div class="content">
        <blockquote>
            <small>
                请确认一下您电子邮箱中的确认链接。<br />
                您注册的资料已经提交，不过我们还需要确认您的电子邮箱地址。<br />
            </small>
        </blockquote>
        <?= Html::a('重发注册确认信息', ['/account/confirm/'], ['class'=>'button']) ?>
    </div>
    <div class="buttons">
        <div class="right"><a href="/" class="button">返回首页</a></div>
    </div>