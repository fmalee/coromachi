<?php

use yii\helpers\Html;

$this->title = '会员中心';
$actions = [
	['Register', ['/account/register'], 'Register a new account'],
	['Resend', ['/account/confirm/index'], 'Resend confirmation token'],
	['Confirm', ['/account/confirm/resend'], 'Confirm a account (needs id and token query params)'],
	['Login', ['/account/default/login'], 'Displays login form'],
	['Logout', ['/account/default/logout'], 'Logs the account out (POST only)'],
	['Recovery', ['/account/recovery/index'], 'Request new recovery token'],
	['Reset', ['/account/recovery/reset'], 'Reset password (needs id and token query params)'],
	['Admin', ['/account/admin'], 'Administrator panel'],
];
$this->params['breadcrumbs'][] = $this->title;
$this->params['body'] = 'account';
?>
<div class="alert alert-info"><strong>Version info: Yii2-account <?= \Yii::$app->getModule('account')->loginType ?></strong>
	<p>This page has a list of available actions for Yii2-account module</p></div>
<table class="table">
	<tr>
		<th>Action</th>
		<th>Description</th>
	</tr>
	<?php foreach ($actions as $action): ?>
		<tr>
			<td><?= \yii\helpers\Html::a($action[0], $action[1]) ?></td>
			<td><?= $action[2] ?></td>
		</tr>
	<?php endforeach ?>
</table>
<?php $this->beginBlock('sidebar') ?>
<div id="column-right" class="sidebar">
    <div class="box">
        <h3 class="box-heading"><span>会员中心</span></h3>
        <div class="box-content">
            <ul class="box-category list">
                <li class="haschild">
                    <a herf="#" class="active">订单管理</a>
                    <ul>
                        <li><a href="category&amp;id=21" title="Vestibulum massa">我的订单</a></li>
                        <li><a href="category&amp;id=23" title="Placerat egestas">我的收藏</a></li>
                        <li><a href="category&amp;id=20" title="Pharetra ultrices">退货的列表</a></li>
                    </ul>
                </li>
                <li class="haschild">
                    <a herf="#" class="active">个人设置</a>
                    <ul>
                        <li><a href="category&amp;id=21" title="Vestibulum massa">个人资料</a></li>
                        <li><a href="category&amp;id=23" title="Placerat egestas">修改密码</a></li>
                        <li><a href="category&amp;id=20" title="Pharetra ultrices">发货地址</a></li>
                        <li><a href="category&amp;id=20" title="Pharetra ultrices">退换服务</a></li>
                        <li><a href="category&amp;id=20" title="Pharetra ultrices">数据包下载</a></li>
                        <li><a href="category&amp;id=20" title="Pharetra ultrices">退货的列表</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<?php $this->endBlock() ?>