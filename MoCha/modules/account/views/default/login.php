<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = '用户登录';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="login-content">
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="inner">
                    <div class="content">
                        <a href=""><img src="/statics/images/demo/login_xyg.jpg"></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="inner">
                    <div class="col-lg-offset-3 col-lg-10"><h1><?= Html::encode($this->title) ?></h1></div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'options' => ['class' => 'form-horizontal'],
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-lg-4\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
                            'labelOptions' => ['class' => 'col-lg-3 control-label'],
                        ],
                    ]); ?>

                    <?= $form->field($model, 'login') ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>

                    <?php if (in_array('login', Yii::$app->getModule('account')->captcha)): ?>
                        <?= $form->field($model, 'verifyCode')
                        ->widget(\yii\captcha\Captcha::className(), [
                            'captchaAction' => 'account/default/captcha',
                            'options' => ['class' => 'form-control'],
                        ]) ?>
                    <?php endif ?>

                    <?= $form->field($model, 'rememberMe', [
                        'template' => "<div class=\"col-lg-offset-3 col-lg-5\">{input}</div>",
                    ])->checkbox() ?>

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-10">
                                <?= Html::submitButton('登 录', ['class' => 'btn btn-primary']) ?> |
                                <?= Html::a('忘记密码？', ['/account/recovery/']) ?> |
                                <?= Html::a('没有收到密码重置的邮件？', ['/account/registration/resend']) ?>
                            </div>

                        </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>