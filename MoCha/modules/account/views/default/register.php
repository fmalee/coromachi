<?php

use yii\widgets\ActiveForm;
use app\components\Ya;

$this->title = '用户注册';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="login-content">
        <div class="row">
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="inner">
                    <div class="col-lg-offset-4 col-lg-8"><h1><?= Ya::encode($this->title) ?></h1></div>
                    <?php $form = ActiveForm::begin([
                        'id' => 'registration-form',
                        'options' => ['class' => 'form-horizontal'],
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-lg-4\">{input}</div>\n<div class=\"col-lg-5\">{error}</div>",
                            'labelOptions' => ['class' => 'col-lg-3 control-label'],
                        ],
                    ]); ?>

                    <?= $form->field($model, 'username') ?>
                    <?= $form->field($model, 'email') ?>

                    <?php if (!Yii::$app->getModule('account')->generatePassword): ?>
                        <?= $form->field($model, 'password')->passwordInput() ?>
                        <?= $form->field($model, 'repeat_password')->passwordInput() ?>
                    <?php endif ?>

                    <?php if (in_array('register', Yii::$app->getModule('account')->captcha)): ?>
                        <?= $form->field($model, 'verifyCode')->widget(\yii\captcha\Captcha::className(), ['template' => '{input}{image}']) ?>
                    <?php endif ?>

                    <div class="form-group">
                        <div class="col-lg-offset-3 col-lg-10">
                            <?= Ya::submitButton('注 册', ['class' => 'btn btn-primary']) ?> |
                            <?= Ya::a('忘记密码？', ['/account/recovery/request']) ?> |
                            <?= Ya::a('没有收到密码重置的邮件？', ['/account/registration/resend']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 col-xs-12">
                <div class="inner">
                    <div class="content">
                        <a href=""><img src="/statics/images/demo/yzsm.gif"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>