<?php

use yii\helpers\Html;

/**
 * @var dektrium\user\models\UserInterface $user
 */
?>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
	亲爱的<?= $user->username?>:
</p>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
	您申请重置在<?= Yii::$app->name?>的账号，如非本人操作，请忽略此邮件。
	立即重置密码，<?= Html::a('请点击这里', $user->getRecoveryUrl()); ?>
	如果上面的链接无法点击，您可以复制下面的地址，并粘帖到浏览器的地址栏中访问。
</p>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
	<?= Html::a(Html::encode($user->getRecoveryUrl()), $user->getRecoveryUrl()); ?>
</p>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
	祝您使用愉快！
</p>