<?php

/**
 * @var string $password
 * @var dektrium\user\models\User $user
 */
?>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
	亲爱的<?= $user->username?>:
</p>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
	您申请重置在<?= Yii::$app->name?>的登录邮箱，如非本人操作，请忽略此邮件。
	请记住以下账户信息，以方便您下次登录:<br>
	账户 : <?= $user->username ?><br>
	密码 : <?= $password ?>.
</p>