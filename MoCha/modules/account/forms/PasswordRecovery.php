<?php

namespace app\modules\account\forms;

use yii\base\InvalidParamException;
use yii\base\Model;
use app\models\User;
/**
 * Model for collecting data on password recovery.
 *
 * @property \app\modules\account\Module $module
 *
 */
class PasswordRecovery extends Model
{
	/**
	 * @var string
	 */
	public $password;

	/**
	 * @var integer
	 */
	public $id;

	/**
	 * @var string
	 */
	public $token;

	/**
	 * @var \app\models\User
	 */
	private $_user;

	/**
	 * @inheritdoc
	 * @throws \yii\base\InvalidParamException
	 */
	public function init()
	{
		parent::init();
		if ($this->id == null || $this->token == null) {
			throw new \RuntimeException('请输入完整的密码重置令牌');
		}
		
		$this->_user = User::find(['id' => $this->id, 'recovery_token' => $this->token]);
		if (!$this->_user) {
			throw new InvalidParamException('这是一个无效的密码重置令牌');
		}
		if ($this->_user->isRecoveryPeriodExpired) {
			throw new InvalidParamException('密码重置令牌已经过期');
		}
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'password' => '新密码',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		return [
			'default' => ['password']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['password', 'required'],
			['password', 'string', 'min' => 6],
		];
	}

	/**
	 * Resets user's password.
	 *
	 * @return bool
	 */
	public function resetPassword()
	{
		if ($this->validate()) {
			$this->_user->reset($this->password);
			return true;
		}

		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function formName()
	{
		return 'recovery-form';
	}


	/**
	 * @return null|\app\modules\account\Module
	 */
	protected function getModule()
	{
		return \Yii::$app->getModule('account');
	}
}