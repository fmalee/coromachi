<?php

namespace app\modules\account\forms;

use yii\base\Model;
use app\models\User;
/**
 * Model that manages resending confirmation tokens to users.
 *
 */
class Resend extends Model
{
	/**
	 * @var string
	 */
	public $email;

	/**
	 * @var string
	 */
	public $verifyCode;

	/**
	 * @var \app\models\UserInterface
	 */
	private $_user;

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'email' => '电子邮箱',
			'verifyCode' => '验证码',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$rules = [
			['email', 'required'],
			['email', 'email'],
			['email', 'exist', 'targetClass' => '\app\models\User', 'message' => '无效的电子邮箱'],
			['email', 'validateEmail'],
		];

		if (in_array('resend', $this->getModule()->captcha)) {
			$rules[] = ['verifyCode', 'captcha', 'captchaAction' => 'site/captcha'];
		}

		return $rules;
	}

	/**
	 * Validates if user has already been confirmed or not.
	 */
	public function validateEmail()
	{
		if ($this->getUser() != null && $this->getUser()->getIsConfirmed()) {
			$this->addError('email', '您的电子邮箱已经进行过确认');
		}
	}

	/**
	 * @inheritdoc
	 */
	public function formName()
	{
		return 'resend-form';
	}

	/**
	 * @return \app\models\UserInterface
	 */
	public function getUser()
	{
		if ($this->_user == null) {
			$this->_user = User::find()->where(['email' => $this->email])->one();
		}

		return $this->_user;
	}

	/**
	 * @return null|\app\modules\account\Module
	 */
	protected function getModule()
	{
		return \Yii::$app->getModule('account');
	}
}
