<?php

namespace app\modules\account\forms;

use yii\base\Model;
use app\models\User;
/**
 * Model for collecting data on password recovery init.
 *
 * @property \app\user\Module $module
 *
 */
class PasswordRecoveryRequest extends Model
{
    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $verifyCode;

    /**
     * @var \app\models\UserInterface
     */
    private $_user;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email' => '电子邮箱',
            'verifyCode' => '验证码',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist', 'targetClass' => '\app\models\User', 'message' => '无效的电子邮箱'],
            ['email', function ($attribute) {
                $this->_user = User::find()->where(['email' => $this->email])->one();
                if ($this->_user !== null && $this->getModule()->confirmable && !$this->_user->getIsConfirmed()) {
                    $this->addError($attribute, '请先确认电子邮箱地址');
                }
            }],
            ['verifyCode', 'captcha',
                'captchaAction' => 'site/captcha',
                'skipOnEmpty' => !in_array('recovery', $this->module->captcha)
            ]
        ];
    }

    /**
     * Sends recovery message.
     *
     * @return bool
     */
    public function sendRecoveryMessage()
    {
        if ($this->validate()) {
            $this->_user->sendRecoveryMessage();
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return 'recovery-request-form';
    }

    /**
     * @return null|\app\account\Module
     */
    protected function getModule()
    {
        return \Yii::$app->getModule('account');
    }
}