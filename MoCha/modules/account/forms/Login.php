<?php

/*
* This file is part of the Dektrium project.
*
* (c) Dektrium project <http://github.com/dektrium/>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

namespace app\modules\account\forms;

use yii\base\Model;
use yii\helpers\Security;
use app\models\UserQuery;
use app\models\User;

/**
 * LoginForm is the model behind the login form.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class Login extends Model
{
	/**
	 * @var string
	 */
	public $login;

	/**
	 * @var string
	 */
	public $password;

	/**
	 * @var bool Whether to remember the user.
	 */
	public $rememberMe = false;

	/**
	 * @var string
	 */
	public $verifyCode;

	/**
	 * @var \app\models\User
	 */
	protected $user;

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		switch ($this->getModule()->loginType) {
			case 'email':
				$loginLabel = '邮箱';
				break;
			case 'username':
				$loginLabel = '用户名';
				break;
			case 'both':
				$loginLabel = '用户名/邮箱';
				break;
			default:
				throw new \RuntimeException;
		}

		return [
			'login'      => $loginLabel,
			'password'   => '密码',
			'rememberMe' => '自动登录',
			'verifyCode' => '验证码',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		$rules = [
			[['login', 'password'], 'required'],
			['password', 'validatePassword'],
			['login', 'validateUserIsConfirmed'],
			['login', 'validateUserIsNotBlocked'],
			['rememberMe', 'boolean'],
		];

		if (in_array('login', $this->getModule()->captcha)) {
			$rules[] = ['verifyCode', 'captcha', 'captchaAction' => 'account/default/captcha'];
		}

		return $rules;
	}

	/**
	 * Validates the password.
	 */
	public function validatePassword()
	{
		if ($this->user === null || !Security::validatePassword($this->password, $this->user->password_hash)) {
			$this->addError('password', '账户或是密码不正确！');
		}
	}

	/**
	 * Validates whether user has confirmed his account.
	 */
	public function validateUserIsConfirmed()
	{
		$confirmationRequired = $this->getModule()->confirmable && !$this->getModule()->allowUnconfirmedLogin;
		if ($this->user !== null && $confirmationRequired && !$this->user->isConfirmed) {
			$this->addError('login', '请先到注册邮箱完成注册确认！');
		}
	}

	/**
	 * Validates whether user is not blocked
	 */
	public function validateUserIsNotBlocked()
	{
		if ($this->user !== null && $this->user->getIsBlocked()) {
			$this->addError('login', '您的账户已经禁用');
		}
	}

	/**
	 * Logs in a user using the provided username and password.
	 *
	 * @return boolean whether the user is logged in successfully
	 */
	public function login()
	{
		if ($this->validate()) {
			return \Yii::$app->getUser()->login($this->user, $this->rememberMe ? $this->getModule()->rememberFor : 0);
		} else {
			return false;
		}
	}

	/**
	 * @inheritdoc
	 */
	public function formName()
	{
		return 'login-form';
	}

	/**
	 * @inheritdoc
	 */
	public function beforeValidate()
	{
		if (parent::beforeValidate()) {
			//$model = new UserQuery();
			//$query = $this->getModule()->factory->createUserQuery();
			//dump($model);
			switch ($this->getModule()->loginType) {
				case 'email':
					$condition = ['email' => $this->login];
					break;
				case 'username':
					$condition = ['username' => $this->login];
					break;
				case 'both':
					$condition = ['or', ['email' => $this->login], ['username' => $this->login]];
					break;
				default:
					throw new \RuntimeException('Unknown login type');
			}
			//$this->user = $model->where($condition)->one();
			$this->user = User::find()->where($condition)->one();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return null|\app\modules\account\Module
	 */
	protected function getModule()
	{
		return \Yii::$app->getModule('account');
	}
}
