Yii2-User
=========

基于[dektrium‘s Yii2-User](https://github.com/dektrium/yii2-user)编写

Yii2-User is a flexible user management module for Yii2 that handles common tasks such as registration, authentication
and password retrieval.

Features include:

* Registration support, with an optional confirmation per mail
* Authentication support
* Password recovery support
* Console commands
* User management (create, update, delete, confirm)
* Unit and functional tests

**NOTE:** Module is in initial development. Anything may change at any time.

## Documentation

You can view the Yii2-user documentation in markdown format here:
[Read the documentation](docs/index.md)

## Installation

All installation instructions are located in [installation guide](docs/installation.md).

## Contributing

See contributing instructions in [CONTRIBUTING.md](CONTRIBUTING.md)

## License

Dektrium user module is released under the MIT License. See the bundled `LICENSE.md` for details.

## What is Dektrium?

The goal of Dektrium project is to provide useful extensions to your Yii2 application to make development easier.