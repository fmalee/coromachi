<?php

namespace app\modules\account\controllers;

use Yii;
use app\controllers\BaseController;
use yii\web\NotFoundHttpException;
use app\models\User;
use app\modules\account\forms\Resend;
/**
 * Controller that manages user registration process.
 *
 * @property \app\modules\account\Module $module
 *
 */
class ConfirmController extends BaseController
{
    public $layout='/single';
    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!$this->module->confirmable) {
                throw new NotFoundHttpException('Disabled by administrator');
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Displays page where user can request new confirmation token.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new Resend();

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->validate()) {
            $model->getUser()->resend();
            Ya::flash('settings_saved', '请查看您的电子邮箱，以进行下一步操作！');
            return $this->render('success', [
                'model' => $model
            ]);
        }

        return $this->render('index', [
            'model' => $model
        ]);
    }

    /**
     * Confirms user's account.
     *
     * @param $id
     * @param $token
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionToken($id, $token)
    {
        $user = User::find(['id' => $id, 'confirmation_token' => $token]);
        if ($user === null) {
            throw new NotFoundHttpException('该用户不存在');
        }
        if ($user->confirm()) {
            return $this->render('finish');
        } else {
            return $this->render('invalidToken');
        }
    }
}
