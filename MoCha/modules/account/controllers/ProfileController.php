<?php

namespace app\modules\account\controllers;

use Yii;
use yii\web\AccessControl;
use app\Controllers\BaseController;
use app\models\Profile;

/**
 * ProfileController shows users profiles.
 *
 * @property \dektrium\user\Module $module
 *
 */
class ProfileController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => ['@']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['show'],
                        'roles' => ['?', '@']
                    ],
                ]
            ],
        ];
    }

    /**
     * Redirects to current user's profile.
     *
     * @return \yii\web\Response
     */
    public function actionIndex()
    {
        return $this->redirect(['show', 'id' => \Yii::$app->getUser()->getId()]);
    }

    /**
     * Shows user's profile.
     *
     * @param $id
     *
     * @return \yii\web\Response
     */
    public function actionShow($id)
    {
        $profile = Profile::find()->where(['user_id' => $id])->with('user')->one();
        return $this->render('show', [
            'profile' => $profile
        ]);
    }
}