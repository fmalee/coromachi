<?php

namespace app\modules\account\controllers;

use Yii;
use app\controllers\BaseController;
use yii\web\AccessControl;
use yii\web\VerbFilter;
use yii\web\NotFoundHttpException;
use app\modules\account\forms\Login;
use app\models\User;

/**
 * Default controller.
 *
 */
class DefaultController extends BaseController
{
    public $layout='/single';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['register', 'login'],
                        'roles' => ['?']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'logout'],
                        'roles' => ['@']
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post']
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        $this->layout = '/left';
        return $this->render('index');
    }

    /**
     * Displays the registration page.
     *
     * @return string
     */
    public function actionRegister()
    {
        $model = new User(['scenario' => 'register']);

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->register()) {
            return $this->render('success', [
                'model' => $model
            ]);
        }

        return $this->render('register', [
            'model' => $model
        ]);
    }

    /**
     * Displays the login page.
     *
     * @return string|\yii\web\Response
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(\Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model
        ]);
    }

    /**
     * Logs the user out and then redirects to the homepage.
     *
     * @return \yii\web\Response
     */
    public function actionLogout()
    {
        \Yii::$app->getUser()->logout();
        return $this->goHome();
    }
}