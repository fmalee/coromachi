<?php

namespace app\modules\account\controllers;

use yii\web\AccessControl;
use app\Controllers\BaseController;
use yii\web\NotFoundHttpException;
use yii\web\VerbFilter;
use app\models\User;
use app\models\Profile;
use Yii;
use app\components\Ya;
/**
 * SettingsController manages updating user settings (e.g. profile, email and password).
 *
 * @property \app\modules\account\Module $module
 *
 */
class SettingsController extends BaseController
{
    /**
     * @inheritdoc
     */
    public $defaultAction = 'profile';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'reset' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['profile', 'email', 'password', 'reset'],
                        'roles' => ['@']
                    ],
                ]
            ],
        ];
    }

    /**
     * Shows profile settings form.
     *
     * @return string|\yii\web\Response
     */
    public function actionProfile()
    {
        $user_id = Yii::$app->getUser()->getIdentity()->getId();
        $model = Profile::find($user_id);
        dump($model);

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->save()) {
            Ya::flash('settings_saved', '您的资料已经更新！');
            return $this->refresh();
        }

        return $this->render('profile', [
            'model' => $model
        ]);
    }

    /**
     * Shows email settings form.
     * TODO:邮箱验证的问题。
     * @return string|\yii\web\Response
     */
    public function actionEmail()
    {
        $id = Yii::$app->getUser()->getIdentity()->getId();
        $model = User::find($id);
        $model->scenario = 'emailSettings';

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->updateEmail()) {
            $this->refresh();
        }

        return $this->render('email', [
            'model' => $model
        ]);
    }

    /**
     * Resets email update.
     *
     * @return \yii\web\Response
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReset()
    {
        if ($this->module->confirmable) {
            $id = Yii::$app->getUser()->getIdentity()->getId();
            $model = User::find($id);
            $model->resetEmailUpdate();
            Ya::flash('settings_saved', '已经取消对邮箱的修改！');
            return $this->redirect(['email']);
        }

        throw new NotFoundHttpException;
    }

    /**
     * Shows password settings form.
     *
     * @return string|\yii\web\Response
     */
    public function actionPassword()
    {
        $id = Yii::$app->getUser()->getIdentity()->getId();
        $model = User::find($id);
        $model->scenario = 'passwordSettings';

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->save()) {
            Ya::flash('settings_saved', '密码修改成功，下次登录请用新密码！');
            $this->refresh();
        }

        return $this->render('password', [
            'model' => $model
        ]);
    }
}