<?php

namespace app\modules\account\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use app\models\User;
use app\modules\account\forms\PasswordRecovery;
use app\modules\account\forms\PasswordRecoveryRequest;

/**
 * RecoveryController manages password recovery process.
 *
 * @property \app\modules\account\Module $module
 *
 */
class RecoveryController extends Controller
{
    public $layout='/single';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'reset'],
                        'roles' => ['?']
                    ],
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!$this->module->recoverable) {
                throw new NotFoundHttpException('管理员已经禁用密码找回功能');
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * Displays page where user can request new recovery message.
     *
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionIndex()
    {
        $model = new PasswordRecoveryRequest();
        if ($model->load(\Yii::$app->getRequest()->post()) && $model->sendRecoveryMessage()) {
            return $this->render('messageSent', [
                'model' => $model
            ]);
        }

        return $this->render('index', [
            'model' => $model
        ]);
    }

    /**
     * Displays page where user can reset password.
     *
     * @param $id
     * @param $token
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionReset($id, $token)
    {
        try {
            $model = new PasswordRecovery([
                'id' => $id,
                'token' => $token
            ]);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->resetPassword()) {
            return $this->render('finish');
        }

        return $this->render('reset', [
            'model' => $model
        ]);
    }
}