<?php

namespace app\models;

/**
 * This is the model class for table "mc_category".
 *
 * @property string $id
 * @property string $name
 * @property string $title
 * @property string $parent_id
 * @property string $parents
 * @property integer $childs
 * @property string $icon
 * @property integer $status
 * @property integer $module
 * @property integer $page_size
 * @property string $meta_title
 * @property string $keywords
 * @property string $description
 * @property string $template_index
 * @property string $template_lists
 * @property string $template_detail
 * @property string $template_edit
 * @property integer $allow_publish
 * @property integer $display
 * @property integer $reply
 * @property integer $check
 * @property string $sort
 * @property string $created
 * @property string $modified
 */
class Category extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE=1;
    const STATUS_NOACTIVE=0;

    //MODUEL表名称
    const MODULE_ARTICLE = 1;
    const MODULE_PRODUCT = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'title', 'template_index', 'template_lists', 'template_detail', 'template_edit'], 'required'],
            [['parent_id', 'childs', 'icon', 'status', 'module', 'page_size', 'allow_publish', 'display', 'reply', 'check', 'sort', 'created', 'modified'], 'integer'],
            [['name'], 'string', 'max' => 30],
            [['title', 'meta_title'], 'string', 'max' => 50],
            [['parents', 'keywords', 'description'], 'string', 'max' => 255],
            [['template_index', 'template_lists', 'template_detail', 'template_edit'], 'string', 'max' => 100],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '分类ID',
            'name' => '标志',
            'title' => '标题',
            'parent_id' => '上级分类',
            'parents' => '上级分类集合',
            'childs' => '下级分类集合',
            'icon' => '分类图标',
            'status' => '数据状态',
            'module' => '关联模块',
            'page_size' => '列表每页行数',
            'meta_title' => 'SEO的网页标题',
            'keywords' => '关键字',
            'description' => '描述',
            'template_index' => '频道页模板',
            'template_lists' => '列表页模板',
            'template_detail' => '详情页模板',
            'template_edit' => '编辑页模板',
            'allow_publish' => '是否允许发布内容',
            'display' => '可见性',
            'reply' => '是否允许回复',
            'check' => '发布的文章是否需要审核',
            'sort' => '排序（同级有效）',
            'created' => '创建时间',
            'modified' => '更新时间',
        ];
    }

    /**
     * @return Profile $article
     */
    public function getArticle()
    {
        return $this->hasMany(Article::className(), ['category_id' => 'id']);
    }

    /**
     * Finds category by name
     *
     * @param string $name
     * @return static|null
     */
    public static function findByName($name)
    {
        return static::find(['name' => $name, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds categorys by name
     *
     * @param string $name
     * @return static|null
     */
    public static function findByModule($module = self::MODULE_ARTICLE, $name = '')
    {
        $where['module'] = $module;
        if (!empty($name)) {
            $where['name'] = $name;
        }
        
        return static::find()->where($where)->all();
    }

    /**
     * 返回目录树
     *
     * @param string $name
     * @return static|null
     */
    public static function getTree($category_id)
    {
        $rows = static::find()
                    ->where(['like', 'parents', $category_id . '|'])
                    ->asArray()
                    ->all();
        
        $categorys = [];
        foreach ($rows as $category) {
            $categorys[$category['id']] = $category;
        }

        $t = [];
        foreach ($categorys as $id => $item) {
            if ($item['parent_id']) {
                $categorys[$item['parent_id']][$item['id']] = &$categorys[$item['id']];
                $t[] = $id;
            }
        }

        foreach($t as $u) {
            unset($categorys[$u]);
        }

        return $categorys;
    }
}
