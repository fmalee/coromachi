<?php

namespace app\models;

/**
 * This is the model class for table "mc_article".
 *
 * @property string $id
 * @property string $user_id
 * @property string $name
 * @property string $title
 * @property string $category_id
 * @property string $description
 * @property string $content
 * @property integer $position
 * @property string $link_id
 * @property string $cover_id
 * @property integer $display
 * @property integer $attach
 * @property string $view
 * @property string $comment
 * @property integer $level
 * @property integer $status
 * @property string $template
 * @property string $created
 * @property string $modified
 */
class Article extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE=1;
    const STATUS_NOACTIVE=0;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%article}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'category_id', 'position', 'link_id', 'cover_id', 'display', 'attach', 'view', 'comment', 'level', 'status', 'created', 'modified'], 'integer'],
            [['content'], 'required'],
            [['content'], 'string'],
            [['name'], 'string', 'max' => 40],
            [['title'], 'string', 'max' => 80],
            [['description'], 'string', 'max' => 140],
            [['template'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '文档ID',
            'user_id' => '用户',
            'name' => '标识',
            'title' => '标题',
            'category_id' => '分类',
            'description' => '描述',
            'content' => '文章内容',
            'position' => '推荐位',
            'link_id' => '外链',
            'cover_id' => '封面',
            'display' => '可见性',
            'attach' => '附件数量',
            'view' => '浏览量',
            'comment' => '评论数',
            'level' => '优先级',
            'status' => '数据状态',
            'template' => '详情页显示模板',
            'created' => '创建时间',
            'modified' => '更新时间',
        ];
    }

    /**
     * @return Profile $user
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return Profile $category
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
