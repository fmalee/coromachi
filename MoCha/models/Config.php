<?php

namespace app\models;

/**
 * This is the model class for table "mc_config".
 *
 * @property string $id
 * @property string $name
 * @property integer $type
 * @property string $title
 * @property integer $group
 * @property string $extra
 * @property string $remark
 * @property string $created
 * @property string $modified
 * @property integer $status
 * @property string $value
 * @property integer $sort
 */
class Config extends \yii\db\ActiveRecord
{
	/**
	 * @ CONST OF GROUP
	 */
	const GROUP_APP = 0;
	const GROUP_SYSTEM = 1;
	const GROUP_ARTICLE = 2;
	const GROUP_ACCOUNT = 3;
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%config}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['type', 'group', 'created', 'modified', 'status', 'sort'], 'integer'],
			[['remark', 'value'], 'required'],
			[['value'], 'string'],
			[['name'], 'string', 'max' => 30],
			[['title'], 'string', 'max' => 50],
			[['extra'], 'string', 'max' => 255],
			[['remark'], 'string', 'max' => 100],
			[['name'], 'unique']
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'id' => '配置ID',
			'name' => '配置名称',
			'type' => '配置类型',
			'title' => '配置说明',
			'group' => '配置分组',
			'extra' => '配置值',
			'remark' => '配置说明',
			'created' => '创建时间',
			'modified' => '更新时间',
			'status' => '状态',
			'value' => '配置值',
			'sort' => '排序',
		];
	}
}
