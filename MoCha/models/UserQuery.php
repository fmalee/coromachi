<?php

namespace app\models;

use yii\db\ActiveQuery;

/**
 * UserQuery
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com
 */
class UserQuery extends ActiveQuery
{
	public $modelClass = '\app\models\User';
	/**
	 * Only confirmed users.
	 *
	 * @return $this
	 */
	public function confirmed()
	{
		$this->andWhere('confirmed_at IS NOT NULL');

		return $this;
	}

	/**
	 * Only unconfirmed users.
	 *
	 * @return $this
	 */
	public function unconfirmed()
	{
		$this->andWhere('confirmed_at IS NULL');

		return $this;
	}
}