<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Article;

/**
 * ArticleSearch represents the model behind the search form about `app\models\Article`.
 */
class ArticleSearch extends Model
{
    public $id;
    public $user_id;
    public $name;
    public $title;
    public $category_id;
    public $description;
    public $content;
    public $position;
    public $link_id;
    public $cover_id;
    public $display;
    public $attach;
    public $view;
    public $comment;
    public $level;
    public $status;
    public $template;
    public $created;
    public $modified;

    public function rules()
    {
        return [
            [['id', 'user_id', 'category_id', 'position', 'link_id', 'cover_id', 'display', 'attach', 'view', 'comment', 'level', 'status', 'created', 'modified'], 'integer'],
            [['name', 'title', 'description', 'content', 'template'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '文档ID',
            'user_id' => '用户ID',
            'name' => '标识',
            'title' => '标题',
            'category_id' => '所属分类',
            'description' => '描述',
            'content' => '文章内容',
            'position' => '推荐位',
            'link_id' => '外链',
            'cover_id' => '封面',
            'display' => '可见性',
            'attach' => '附件数量',
            'view' => '浏览量',
            'comment' => '评论数',
            'level' => '优先级',
            'status' => '数据状态',
            'template' => '详情页显示模板',
            'created' => '创建时间',
            'modified' => '更新时间',
        ];
    }

    public function search($params)
    {
        $query = Article::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $this->addCondition($query, 'id');
        $this->addCondition($query, 'user_id');
        $this->addCondition($query, 'name', true);
        $this->addCondition($query, 'title', true);
        $this->addCondition($query, 'category_id');
        $this->addCondition($query, 'description', true);
        $this->addCondition($query, 'content', true);
        $this->addCondition($query, 'position');
        $this->addCondition($query, 'link_id');
        $this->addCondition($query, 'cover_id');
        $this->addCondition($query, 'display');
        $this->addCondition($query, 'attach');
        $this->addCondition($query, 'view');
        $this->addCondition($query, 'comment');
        $this->addCondition($query, 'level');
        $this->addCondition($query, 'status');
        $this->addCondition($query, 'template', true);
        $this->addCondition($query, 'created');
        $this->addCondition($query, 'modified');
        return $dataProvider;
    }

    protected function addCondition($query, $attribute, $partialMatch = false)
    {
        if (($pos = strrpos($attribute, '.')) !== false) {
            $modelAttribute = substr($attribute, $pos + 1);
        } else {
            $modelAttribute = $attribute;
        }

        $value = $this->$modelAttribute;
        if (trim($value) === '') {
            return;
        }
        if ($partialMatch) {
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }
}
