<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "profile".
 *
 * @property integer $user_id
 * @property string $name
 * @property integer $sex
 * @property string $birthday
 * @property string $area_id
 * @property string $location
 * @property string $qq
 * @property string $public_email
 * @property string $gravatar_email
 * @property string $gravatar_id
 * @property string $website
 * @property string $bio
 *
 * @property User $user
 */
class Profile extends ActiveRecord
{
	/**
	 * @inheritdoc
	 */
	public static function tableName()
	{
		return '{{%profile}}';
	}

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['user_id'], 'required'],
			[['user_id', 'sex', 'area_id'], 'integer'],
			[['public_email', 'gravatar_email'], 'email'],
			[['birthday'], 'safe'],
			[['bio'], 'string'],
			[['name', 'location', 'public_email', 'gravatar_email', 'website'], 'string', 'max' => 255],
			[['qq'], 'string', 'max' => 10],
			[['gravatar_id'], 'string', 'max' => 32]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function attributeLabels()
	{
		return [
			'user_id' => 'User ID',
			'name' => '姓名',
			'sex' => '性别',
			'birthday' => '生日',
			'area_id' => '所有地区',
			'location' => '所在地址',
			'qq' => 'QQ号',
			'public_email' => '公开邮箱',
			'gravatar_email' => 'gravatar账户',
			'gravatar_id' => 'gravatar‘s ID',
			'website' => '网站',
			'bio' => 'Bio',
		];
	}

	/**
	 * @inheritdoc
	 */
	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if ($this->isAttributeChanged('gravatar_email')) {
				$this->setAttribute('gravatar_id', md5($this->getAttribute('gravatar_email')));
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return \yii\db\ActiveQueryInterface
	 */
	public function getUser()
	{
		return $this->hasOne(User::className(), ['id' => 'user_id']);
	}
}
