<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Category;

/**
 * CategorySearch represents the model behind the search form about `app\models\Category`.
 */
class CategorySearch extends Model
{
    public $id;
    public $name;
    public $title;
    public $parent_id;
    public $arr_parent;
    public $icon;
    public $status;
    public $module;
    public $page_size;
    public $meta_title;
    public $keywords;
    public $description;
    public $template_index;
    public $template_lists;
    public $template_detail;
    public $template_edit;
    public $allow_publish;
    public $display;
    public $reply;
    public $check;
    public $sort;
    public $created;
    public $modified;

    public function rules()
    {
        return [
            [['id', 'parent_id', 'icon', 'status', 'module', 'page_size', 'allow_publish', 'display', 'reply', 'check', 'sort', 'created', 'modified'], 'integer'],
            [['name', 'title', 'arr_parent', 'meta_title', 'keywords', 'description', 'template_index', 'template_lists', 'template_detail', 'template_edit'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '分类ID',
            'name' => '标志',
            'title' => '标题',
            'parent_id' => '上级分类',
            'arr_parent' => '上级分类集合',
            'icon' => '分类图标',
            'status' => '数据状态',
            'module' => '关联模块',
            'page_size' => '列表每页行数',
            'meta_title' => 'SEO的网页标题',
            'keywords' => '关键字',
            'description' => '描述',
            'template_index' => '频道页模板',
            'template_lists' => '列表页模板',
            'template_detail' => '详情页模板',
            'template_edit' => '编辑页模板',
            'allow_publish' => '是否允许发布内容',
            'display' => '可见性',
            'reply' => '是否允许回复',
            'check' => '发布的文章是否需要审核',
            'sort' => '排序（同级有效）',
            'created' => '创建时间',
            'modified' => '更新时间',
        ];
    }

    public function search($params)
    {
        $query = Category::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $this->addCondition($query, 'id');
        $this->addCondition($query, 'name', true);
        $this->addCondition($query, 'title', true);
        $this->addCondition($query, 'parent_id');
        $this->addCondition($query, 'arr_parent', true);
        $this->addCondition($query, 'icon');
        $this->addCondition($query, 'status');
        $this->addCondition($query, 'module');
        $this->addCondition($query, 'page_size');
        $this->addCondition($query, 'meta_title', true);
        $this->addCondition($query, 'keywords', true);
        $this->addCondition($query, 'description', true);
        $this->addCondition($query, 'template_index', true);
        $this->addCondition($query, 'template_lists', true);
        $this->addCondition($query, 'template_detail', true);
        $this->addCondition($query, 'template_edit', true);
        $this->addCondition($query, 'allow_publish');
        $this->addCondition($query, 'display');
        $this->addCondition($query, 'reply');
        $this->addCondition($query, 'check');
        $this->addCondition($query, 'sort');
        $this->addCondition($query, 'created');
        $this->addCondition($query, 'modified');
        return $dataProvider;
    }

    protected function addCondition($query, $attribute, $partialMatch = false)
    {
        if (($pos = strrpos($attribute, '.')) !== false) {
            $modelAttribute = substr($attribute, $pos + 1);
        } else {
            $modelAttribute = $attribute;
        }

        $value = $this->$modelAttribute;
        if (trim($value) === '') {
            return;
        }
        if ($partialMatch) {
            $query->andWhere(['like', $attribute, $value]);
        } else {
            $query->andWhere([$attribute => $value]);
        }
    }
}
